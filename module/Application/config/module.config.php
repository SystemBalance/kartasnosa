<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Route\StaticRoute;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'router' => [
        'routes' => [
        
            // 1. Главная страница (список районов, округов, улиц по алфавиту со ссылкой на отдельные страницы)
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            // 2. Страница округа (список домов, попавших под снос, список районов и улиц окргуа)
            'district' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/okrug[/:url]',
                    'constraints' => [
//                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                        'action' => 'index',
                        'url' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller'    => Controller\DistrictController::class,
                        'action'        => 'view',
                    ],
                ],
            ],
            'districts' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/okrug/',
                    'constraints' => [
//                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => 'index',
                    ],
                    'defaults' => [
                        'controller'    => Controller\DistrictController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            // 3. Страница района (список домов, попавших под снос, список улиц)
            'region' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/rayon[/:url]',
                    'constraints' => [
//                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                        'action' => 'index',
                        'url' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller'    => Controller\RegionController::class,
                        'action'        => 'view',
                    ],
                ],
            ],            // 3. Страница района (список домов, попавших под снос, список улиц)
            'regions' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/rayon/',
                    'constraints' => [
//                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                        'action' => 'index',
                    ],
                    'defaults' => [
                        'controller'    => Controller\RegionController::class,
                        'action'        => 'index',
                    ],
                ],
            ],

            // 4. Страница улицы (список домов, попавших под снос, список не попавших)
            'street' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/street[/:url]',
                    'constraints' => [
                        'url' => '[a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller'    => Controller\StreetController::class,
                        'action'        => 'view',
                    ],
                ],
            ],
            'streets' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/street/',
                    'constraints' => [
                    ],
                    'defaults' => [
                        'controller'    => Controller\StreetController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            'getStreetJson' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/street/json',
                    'constraints' => [
                    ],
                    'defaults' => [
                        'controller'    => Controller\StreetController::class,
                        'action'        => 'getJson',
                    ],
                ],
            ],


            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller'    => Controller\IndexController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            'house' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/house[/:url]',
                    'constraints' => [
                        'url' => '[a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller'    => Controller\HouseController::class,
                        'action'        => 'view',
                    ],
                ],
            ],
            'houses' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/house/',
                    'constraints' => [
                    ],
                    'defaults' => [
                        'controller'    => Controller\HouseController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            'mapData' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/mapData[/:route[/:url]]',
                    'constraints' => [
                        'route' => '[a-zA-Z]+',
                        'url' => '[a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller'    => Controller\HouseController::class,
                        'action'        => 'mapData',
                        'route'          => 'main',
                        'url'          => ''
                    ],
                ],
            ],
            'houses-jsonbystreet' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/house/jsonbystreet/[:idstreet]',
                    'constraints' => [
                    ],
                    'defaults' => [
                        'controller'    => Controller\HouseController::class,
                        'action'        => 'getDataByStreet',
                    ],
                ],
            ],
            'about' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/about',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'about',
                    ],
                ],
            ],                                    
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => Controller\Factory\IndexControllerFactory::class,
            Controller\HouseController::class => Controller\Factory\HouseControllerFactory::class,
            Controller\DistrictController::class => Controller\Factory\DistrictControllerFactory::class,
            Controller\RegionController::class => Controller\Factory\RegionControllerFactory::class,
            Controller\StreetController::class => Controller\Factory\StreetControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\HouseManager::class => Service\Factory\HouseManagerFactory::class,
        ],
    ],
    
    // The following registers our custom view 
    // helper classes in view plugin manager.
    'view_helpers' => [
        'factories' => [
            View\Helper\Menu::class => InvokableFactory::class,
            View\Helper\Breadcrumbs::class => InvokableFactory::class,
        ],
        'aliases' => [
            'mainMenu' => View\Helper\Menu::class,
            'pageBreadcrumbs' => View\Helper\Breadcrumbs::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
];
