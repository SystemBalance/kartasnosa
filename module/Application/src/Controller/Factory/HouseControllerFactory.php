<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\HouseManager;
use Application\Controller\HouseController;

/**
 * This is the factory for HouseController. Its purpose is to instantiate the
 * controller.
 */
class HouseControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $HouseManager = $container->get(HouseManager::class);
        
        // Instantiate the controller and inject dependencies
        return new HouseController($entityManager, $HouseManager);
    }
}


