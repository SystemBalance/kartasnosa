<?php
namespace Application\Controller;

use Application\Entity\District;
use Application\Entity\Region;
use Application\Entity\Street;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Helper\Json;
use Zend\View\Model\ConsoleModel;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Application\Entity\House;


/**
 * This is the House controller class of the Blog application. 
 * This controller is used for managing House (adding/editing/viewing/deleting).
 */
class HouseController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    private $entityManager;
    
    /**
     * House manager.
     * @var Application\Service\HouseManager 
     */
    private $HouseManager;
    
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * This action displays the "View Street" page allowing to see the Street title
     * and content. The page also contains a form allowing
     * to add a comment to Street.
     */
    public function viewAction()
    {
        $houseUrl = $this->params()->fromRoute('url', null);


        // Find the Street by ID
        $House = $this->entityManager->getRepository(House::class)
            ->findOneByUrl($houseUrl);

        if ($House == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->layout()->centerLat = $House->getLat();
        $this->layout()->centerLng = $House->getLng();
        $this->layout()->centerZoom = '18';

        // Render the view template.
        return new ViewModel([
            'House' => $House,
            'Street' => $House->getIdStreet()
        ]);
    }

    /**
     * All map jsons
     * @return JsonModel
     */
    public function mapDataAction(){
        $route = $this->params()->fromRoute('route', 'home');
        $url = $this->params()->fromRoute('url', '');

        $streetId = [];
        $Houses = null;


        //+
        if($route == 'district'){
            $District = $this->entityManager->getRepository(District::class)->findOneByUrl($url);

            $Regions = $this->entityManager->getRepository(Region::class)->findBy(['idDistrict'=>$District->getIdDistrict()]);
            $rIds = [];
            foreach ($Regions as $r){
                $rIds[] = $r->getIdRegion();
            }
            $streetId = $this->entityManager->getRepository(Street::class)->getIdListByRegionsId($rIds);
        }

        //+
        if($route == 'region'){
            $Region = $this->entityManager->getRepository(Region::class)->findOneByUrl($url);
            $streetId = $this->entityManager->getRepository(Street::class)->getIdListByRegionsId([$Region->getIdRegion()]);
        }

        //+
        if($route == 'street'){
            $Street = $this->entityManager->getRepository(Street::class)->findOneByUrl($url);
            $streetId[] = $Street->getIdStreet();
        }

        //One
        if($route == 'house'){
            $Houses = [$this->entityManager->getRepository(House::class)->findOneByUrl($url)];
        } elseif($route=='street'||$route=='region'||$route=='district') {
            $Houses = $this->entityManager->getRepository(House::class)->findByIdStreet($streetId);
        } else {
            $Houses = $this->entityManager->getRepository(House::class)->findAll();
        }
        $features = [0=>[],1=>[]];
        foreach ($Houses as $h){

            $features[$h->getRen()][] = [
                "type"=> "Feature",
                "id"=> $h->getIdHouse(),
                "geometry"=> [
                    "type"=> "Point",
                    "coordinates"=> [$h->getLat(), $h->getLng()]
                ],
                "properties"=> [
                    "balloonContentHeader"=> '<a href="'.$this->url()->fromRoute('house',['url'=>$h->getUrl()]).'">'.$h->getAddressFull().'</a>',
                    "balloonContentBody"=> "<div>".
                        (!empty($h->getPostcode())) ? "<div>Индекс: ".$h->getPostcode()."</div>":"".
    (!empty($h->getType())) ? "<div>Тип дома: ".$h->getType()."</div>":"".
    (!empty($h->getYear())) ? "<div>Год постройки: ".$h->getYear()."</div>":"".
    (!empty($h->getFloors())) ? "<div>Этажность: ".$h->getFloors()."</div>":"".
"</div>",
                    "balloonContentFooter"=> "",
                    "clusterCaption"=> '<a href="'.$this->url()->fromRoute('house',['url'=>$h->getUrl()]).'">'.$h->getAddressFull().'</a>',
                    "hintContent"=> $h->getAddressFull()
                ],
                "options"=>[]
            ];
        }
//        return new ViewModel([
        return new JsonModel([
            0=>[
                "type"=> "FeatureCollection",
                "features"=> $features[0]
            ],
            1=>[
                "type"=> "FeatureCollection",
                "features"=> $features[1]
            ],
        ]);
    }

    public function getDataByStreetAction(){
        $idStreet = $this->params()->fromRoute('idstreet');

        $Houses = $this->entityManager->getRepository(House::class)->findBy(['idStreet'=>$idStreet],array('title' => 'ASC'));
        $houseList = [];
        $houseStr = '';
        foreach ($Houses as $h){
            $houseStr.='<option value="' . $h->getUrl() . '">'  .$h->getTitle(). '</option>';
            $houseList[] = [
                'id'=>$h->getIdHouse(),
                'name'=>$h->getTitle()
            ];
        }

        
        return new JsonModel([$houseStr]);
    }

//
//    /**
//     * This action displays the "New House" page. The page contains a form allowing
//     * to enter House title, content and tags. When the user clicks the Submit button,
//     * a new House entity will be created.
//     */
//    public function addAction()
//    {
//        // Create the form.
//        $form = new HouseForm();
//
//        // Check whether this House is a House request.
//        if ($this->getRequest()->isHouse()) {
//
//            // Get House data.
//            $data = $this->params()->fromHouse();
//
//            // Fill form with data.
//            $form->setData($data);
//            if ($form->isValid()) {
//
//                // Get validated form data.
//                $data = $form->getData();
//
//                // Use House manager service to add new House to database.
//                $this->HouseManager->addNewHouse($data);
//
//                // Redirect the user to "index" page.
//                return $this->redirect()->toRoute('application');
//            }
//        }
//
//        // Render the view template.
//        return new ViewModel([
//            'form' => $form
//        ]);
//    }
//
//    /**
//     * This action displays the "View House" page allowing to see the House title
//     * and content. The page also contains a form allowing
//     * to add a comment to House.
//     */
//    public function viewAction()
//    {
//        $HouseId = (int)$this->params()->fromRoute('id', -1);
//
//        // Validate input parameter
//        if ($HouseId<0) {
//            $this->getResponse()->setStatusCode(404);
//            return;
//        }
//
//        // Find the House by ID
//        $House = $this->entityManager->getRepository(House::class)
//                ->findOneById($HouseId);
//
//        if ($House == null) {
//            $this->getResponse()->setStatusCode(404);
//            return;
//        }
//
//        // Create the form.
//        $form = new CommentForm();
//
//        // Check whether this House is a House request.
//        if($this->getRequest()->isHouse()) {
//
//            // Get House data.
//            $data = $this->params()->fromHouse();
//
//            // Fill form with data.
//            $form->setData($data);
//            if($form->isValid()) {
//
//                // Get validated form data.
//                $data = $form->getData();
//
//                // Use House manager service to add new comment to House.
//                $this->HouseManager->addCommentToHouse($House, $data);
//
//                // Redirect the user again to "view" page.
//                return $this->redirect()->toRoute('house', ['action'=>'view', 'id'=>$HouseId]);
//            }
//        }
//
//        // Render the view template.
//        return new ViewModel([
//            'House' => $House,
//            'form' => $form,
//            'HouseManager' => $this->HouseManager
//        ]);
//    }
//
//    /**
//     * This action displays the page allowing to edit a House.
//     */
//    public function editAction()
//    {
//        // Create form.
//        $form = new HouseForm();
//
//        // Get House ID.
//        $HouseId = (int)$this->params()->fromRoute('id', -1);
//
//        // Validate input parameter
//        if ($HouseId<0) {
//            $this->getResponse()->setStatusCode(404);
//            return;
//        }
//
//        // Find the existing House in the database.
//        $House = $this->entityManager->getRepository(House::class)
//                ->findOneById($HouseId);
//        if ($House == null) {
//            $this->getResponse()->setStatusCode(404);
//            return;
//        }
//
//        // Check whether this House is a House request.
//        if ($this->getRequest()->isPost()) {
//
//            // Get House data.
//            $data = $this->params()->fromHouse();
//
//            // Fill form with data.
//            $form->setData($data);
//            if ($form->isValid()) {
//
//                // Get validated form data.
//                $data = $form->getData();
//
//                // Use House manager service update existing House.
//                $this->HouseManager->updateHouse($House, $data);
//
//                // Redirect the user to "admin" page.
//                return $this->redirect()->toRoute('House', ['action'=>'admin']);
//            }
//        } else {
//            $data = [
//                'title' => $House->getTitle(),
////                'content' => $House->getContent(),
//                'tags' => $this->HouseManager->convertTagsToString($House),
//                'status' => $House->getStatus()
//            ];
//
//            $form->setData($data);
//        }
//
//        // Render the view template.
//        return new ViewModel([
//            'form' => $form,
//            'House' => $House
//        ]);
//    }
//
//    /**
//     * This "delete" action deletes the given House.
//     */
//    public function deleteAction()
//    {
//        $HouseId = (int)$this->params()->fromRoute('id', -1);
//
//        // Validate input parameter
//        if ($HouseId<0) {
//            $this->getResponse()->setStatusCode(404);
//            return;
//        }
//
//        $House = $this->entityManager->getRepository(House::class)
//                ->findOneById($HouseId);
//        if ($House == null) {
//            $this->getResponse()->setStatusCode(404);
//            return;
//        }
//
//        $this->HouseManager->removeHouse($House);
//
//        // Redirect the user to "admin" page.
//        return $this->redirect()->toRoute('House', ['action'=>'admin']);
//
//    }
//
//    /**
//     * This "admin" action displays the Manage House page. This page contains
//     * the list of House with an ability to edit/delete any House.
//     */
//    public function adminAction()
//    {
//        // Get recent House
//        $House = $this->entityManager->getRepository(House::class)->findAll();
////                ->findBy([], ['dateCreated'=>'DESC']);
//
//        // Render the view template
//        return new ViewModel([
//            'House' => $House,
//            'HouseManager' => $this->HouseManager
//        ]);
//    }
}