<?php
namespace Application\Controller;

use Application\Entity\District;
use Application\Entity\Region;
use Application\Entity\Street;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;
use Application\Entity\House;


/**
 * This is the main controller class of the Blog application. The 
 * controller class is used to receive user input,  
 * pass the data to the models and pass the results returned by models to the 
 * view for rendering.
 */
class IndexController extends BaseController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    private $entityManager;
    
    /**
     * House manager.
     * @var Application\Service\HouseManager 
     */
    private $HouseManager;
    
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct($entityManager, $HouseManager) 
    {
        $this->entityManager = $entityManager;
        $this->HouseManager = $HouseManager;
    }
    
    /**
     * 1. Главная страница (список районов, округов, улиц по алфавиту со ссылкой на отдельные страницы)
     */
    public function indexAction() 
    {
        $Districts = $this->entityManager->getRepository(District::class)->findAll();
//        $Regions = $this->entityManager->getRepository(Region::class)->findAll();
        $Streets = $this->entityManager->getRepository(Street::class)->findAll();


        $count_of_houses = $this->entityManager->getRepository(House::class)->getCountAll();
        $count_of_houses_ren = $this->entityManager->getRepository(House::class)->getCountAllRen();
//        $page = $this->params()->fromQuery('page', 1);
        // Get recent House
//        $query = $this->entityManager->getRepository(House::class)
//                ->findPublishedHouse();
//
//        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
//        $paginator = new Paginator($adapter);
//
//        $paginator->setDefaultItemCountPerPage(10);
//        $paginator->setCurrentPageNumber($page);


//        $this->layout()->setVariables([
//            'title' => 'Списки домов под снос в Москве по программе реновации 2017',
//        ]);

        // Render the view template.
        return new ViewModel([
            'Districts' => $Districts,
//            'Regions'   => $Regions,
            'Streets'   => $Streets,
            'count_of_houses' => $count_of_houses,
            'count_of_houses_ren' => $count_of_houses_ren,
//            'tagCloud' =>[]// $tagCloud
        ]);
    }
    
    /**
     * This action displays the About page.
     */
    public function aboutAction() 
    {   
        $appName = 'Blog';
        $appDescription = 'A simple blog application for the Using Zend Framework 3 book';
        
        return new ViewModel([
            'appName' => $appName,
            'appDescription' => $appDescription
        ]);
    }
}
