<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * District
 *
 * @ORM\Table(name="district")
 * @ORM\Entity(repositoryClass="\Application\Repository\DistrictRepository")
 */
class District
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_district", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="title_short", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $titleShort;

    /**
     * @var string
     *
     * @ORM\Column(name="ext_url", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $extUrl;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Application\Entity\Region", mappedBy="idDistrict")
     *
     */
    private $regions;

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegions()
    {
        return $this->regions;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $regions
     */
    public function setRegions($regions)
    {
        $this->regions = $regions;
    }


    public function __construct()
    {
        $this->regions = new ArrayCollection();
    }

    /**
     * Get idDistrict
     *
     * @return integer
     */
    public function getIdDistrict()
    {
        return $this->idDistrict;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return District
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return District
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set titleShort
     *
     * @param string $titleShort
     *
     * @return District
     */
    public function setTitleShort($titleShort)
    {
        $this->titleShort = $titleShort;

        return $this;
    }

    /**
     * Get titleShort
     *
     * @return string
     */
    public function getTitleShort()
    {
        return $this->titleShort;
    }

    /**
     * Set extUrl
     *
     * @param string $extUrl
     *
     * @return District
     */
    public function setExtUrl($extUrl)
    {
        $this->extUrl = $extUrl;

        return $this;
    }

    /**
     * Get extUrl
     *
     * @return string
     */
    public function getExtUrl()
    {
        return $this->extUrl;
    }
}

