<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;



/**
 * House
 * @ORM\Table(name="house", indexes={@ORM\Index(name="id_street", columns={"id_street"})}))
 * @ORM\Entity(repositoryClass="\Application\Repository\HouseRepository")
 */
class House
{
    // Post status constants.
    const STATUS_DRAFT       = 1; // Draft.
    const STATUS_PUBLISHED   = 2; // Published.

    /**
     * @var integer
     *
     * @ORM\Column(name="id_house", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idHouse;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="address_full", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $addressFull;

    /**
     * @var integer
     *
     * @ORM\Column(name="postcode", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $postcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $year;

    /**
     * @var integer
     *
     * @ORM\Column(name="floors", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $floors;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="decimal", precision=11, scale=8, nullable=true, unique=false)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="decimal", precision=11, scale=8, nullable=true, unique=false)
     */
    private $lng;

    /**
     * @var string
     *
     * @ORM\Column(name="ext_url", type="string", length=200, precision=0, scale=0, nullable=true, unique=false)
     */
    private $extUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="ext_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $extId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="error", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $error;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $status;

    /**
     * @var \Application\Entity\Street
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Street", inversedBy="houses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_street", referencedColumnName="id_street", nullable=true)
     * })
     */
    private $idStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $url;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }


    /**
     * Renovation flag
     * @var integer
     *
     * @ORM\Column(name="ren", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ren;

    /**
     * @return int
     */
    public function getRen()
    {
        return $this->ren;
    }

    /**
     * @param int $ren
     */
    public function setRen($ren)
    {
        $this->ren = $ren;
    }



    /**
     * Get idHouse
     *
     * @return integer
     */
    public function getIdHouse()
    {
        return $this->idHouse;
    }
    public function getId(){
        return $this->getIdHouse();
    }
    public function setId($id){
        return $this->setIdHouse($id);
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return House
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set addressFull
     *
     * @param string $addressFull
     *
     * @return House
     */
    public function setAddressFull($addressFull)
    {
        $this->addressFull = $addressFull;

        return $this;
    }

    /**
     * Get addressFull
     *
     * @return string
     */
    public function getAddressFull()
    {
        return $this->addressFull;
    }

    /**
     * Set postcode
     *
     * @param integer $postcode
     *
     * @return House
     */
    public function setpostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return integer
     */
    public function getpostcode()
    {
        return $this->postcode;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return House
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set floors
     *
     * @param integer $floors
     *
     * @return House
     */
    public function setFloors($floors)
    {
        $this->floors = $floors;

        return $this;
    }

    /**
     * Get floors
     *
     * @return integer
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return House
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set lat
     *
     * @param string $lat
     *
     * @return House
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param string $lng
     *
     * @return House
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set extUrl
     *
     * @param string $extUrl
     *
     * @return House
     */
    public function setExtUrl($extUrl)
    {
        $this->extUrl = $extUrl;

        return $this;
    }

    /**
     * Get extUrl
     *
     * @return string
     */
    public function getExtUrl()
    {
        return $this->extUrl;
    }

    /**
     * Set extId
     *
     * @param integer $extId
     *
     * @return House
     */
    public function setExtId($extId)
    {
        $this->extId = $extId;

        return $this;
    }

    /**
     * Get extId
     *
     * @return integer
     */
    public function getExtId()
    {
        return $this->extId;
    }

    /**
     * Set error
     *
     * @param boolean $error
     *
     * @return House
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get error
     *
     * @return boolean
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return House
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idStreet
     *
     * @param \Application\Entity\Street $idStreet
     *
     * @return House
     */
    public function setIdStreet(\Application\Entity\Street $idStreet = null)
    {
        $this->idStreet = $idStreet;

        return $this;
    }

    /**
     * Get idStreet
     *
     * @return \Application\Entity\Street
     */
    public function getIdStreet()
    {
        return $this->idStreet;
    }
}

