<?php

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 *
 * @ORM\Table(name="region", indexes={@ORM\Index(name="id_district", columns={"id_district"})})
 * @ORM\Entity(repositoryClass="\Application\Repository\StreetRepository")
 */
class Region
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_region", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="ext_url", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $extUrl;

    /**
     * @var \Application\Entity\District
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\District", inversedBy="regions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_district", referencedColumnName="id_district", nullable=true)
     * })
     */
    private $idDistrict;



    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Application\Entity\Street", mappedBy="idRegion")
     */
    private $streets;

    /**
     * @return \Doctrine\Common\Collections\Collection
     */


    public function __construct()
    {
        $this->streets = new ArrayCollection();
    }


    public function getStreets()
    {
        return $this->streets;
    }

    /**
     * Get idRegion
     *
     * @return integer
     */
    public function getIdRegion()
    {
        return $this->idRegion;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Region
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Region
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set extUrl
     *
     * @param string $extUrl
     *
     * @return Region
     */
    public function setExtUrl($extUrl)
    {
        $this->extUrl = $extUrl;

        return $this;
    }

    /**
     * Get extUrl
     *
     * @return string
     */
    public function getExtUrl()
    {
        return $this->extUrl;
    }

    /**
     * Set idDistrict
     *
     * @param \Application\Entity\District $idDistrict
     *
     * @return Region
     */
    public function setIdDistrict(\Application\Entity\District $idDistrict = null)
    {
        $this->idDistrict = $idDistrict;

        return $this;
    }

    /**
     * Get idDistrict
     *
     * @return \Application\Entity\District
     */
    public function getIdDistrict()
    {
        return $this->idDistrict;
    }
}

