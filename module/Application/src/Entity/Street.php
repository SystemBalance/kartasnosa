<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\House;

/**
 * Street
 *
 * @ORM\Table(name="street", indexes={@ORM\Index(name="id_region", columns={"id_region"})})
 * @ORM\Entity(repositoryClass="\Application\Repository\StreetRepository")
 */

class Street
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_street", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="letter", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $letter;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="extname", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $extname;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="ext_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $extId;

    /**
     * @var string
     *
     * @ORM\Column(name="ext_url", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $extUrl;

    /**
     * @var \Application\Entity\Region
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Region",inversedBy="streets")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_region", referencedColumnName="id_region", nullable=true)
     * })
     */
    private $idRegion;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Application\Entity\House", mappedBy="idStreet")
     */
    private $houses;


    /**
     * @return \Doctrine\Common\Collections\Collection
     */

    public function __construct()
    {
        $this->houses = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getHouses()
    {
        return $this->houses;
    }


    public function getIdStreet()
    {
        return $this->idStreet;
    }

    /**
     * Set letter
     *
     * @param string $letter
     *
     * @return Street
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * Get letter
     *
     * @return string
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Street
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set extname
     *
     * @param string $extname
     *
     * @return Street
     */
    public function setExtname($extname)
    {
        $this->extname = $extname;

        return $this;
    }

    /**
     * Get extname
     *
     * @return string
     */
    public function getExtname()
    {
        return $this->extname;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Street
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set extId
     *
     * @param integer $extId
     *
     * @return Street
     */
    public function setExtId($extId)
    {
        $this->extId = $extId;

        return $this;
    }

    /**
     * Get extId
     *
     * @return integer
     */
    public function getExtId()
    {
        return $this->extId;
    }

    /**
     * Set extUrl
     *
     * @param string $extUrl
     *
     * @return Street
     */
    public function setExtUrl($extUrl)
    {
        $this->extUrl = $extUrl;

        return $this;
    }

    /**
     * Get extUrl
     *
     * @return string
     */
    public function getExtUrl()
    {
        return $this->extUrl;
    }

    /**
     * Set idRegion
     *
     * @param \Application\Entity\Region $idRegion
     *
     * @return Street
     */
    public function setIdRegion(\Application\Entity\Region $idRegion = null)
    {
        $this->idRegion = $idRegion;

        return $this;
    }

    /**
     * Get idRegion
     *
     * @return \Application\Entity\Region
     */
    public function getIdRegion()
    {
        return $this->idRegion;
    }
}

