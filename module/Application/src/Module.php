<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Doctrine\Common\Collections\ArrayCollection;
use Zend\EventManager\Event;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;


class Module
{
    const VERSION = '3.0.0dev';

    // The "init" method is called on application start-up and
    // allows to register an event listener.
    public function init(ModuleManager $manager)
    {
//         Get event manager.
        $eventManager = $manager->getEventManager();
        $sharedEventManager = $eventManager->getSharedManager();
//         Register the event listener method.

//        $sharedEventManager->attach(__NAMESPACE__, MvcEvent::EVENT_ROUTE,
//            [$this, 'onRoute'], 100);
//
        $sharedEventManager->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH,
            [$this, 'onDispatch'], 200);

    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(Event $e){
        $sm = $e->getApplication()->getServiceManager();
        $entityManager = $sm->get('Doctrine\ORM\EntityManager');

        $eventManager = $e->getApplication()->getEventManager();
        $sharedEventManager = $eventManager->getSharedManager();


        $e->getViewModel()->centerLat = '55.76';
        $e->getViewModel()->centerLng = '37.64';
        $e->getViewModel()->centerZoom = '11';

        $regions = $entityManager
            ->getRepository('Application\Entity\Region')
            ->findBy(array(), array('title' => 'ASC'));
        $e->getViewModel()->regions= new ArrayCollection($regions);



        $streets = $entityManager
            ->getRepository('Application\Entity\Street')
            ->findBy(array(), array('title' => 'ASC'));
        $e->getViewModel()->streets= new ArrayCollection($streets);


    }

    public function onDispatch(MvcEvent $e){
        $matches    = $e->getRouteMatch();

        $e->getViewModel()->routeName = $matches->getMatchedRouteName();
        $e->getViewModel()->action     = $matches->getParam('action');
        $e->getViewModel()->controller = $matches->getParam('controller');

        $routeId = $matches->getParam('id');
        $routeUrl = $matches->getParam('url');

        $e->getViewModel()->routeUrl = '';
        if(!empty($routeId)){
            $e->getViewModel()->routeUrl = $routeId;
        }elseif(!empty($routeUrl)){
            $e->getViewModel()->routeUrl = $routeUrl;
        }
    }

    // Event listener method.
    public function onRoute(MvcEvent $event)
    {
        if (php_sapi_name() == "cli") {
            // Do not execute HTTPS redirect in console mode.
            return;
        }

        // Get request URI
        $uri = $event->getRequest()->getUri();
        $scheme = $uri->getScheme();
        // If scheme is not HTTPS, redirect to the same URI, but with
        // HTTPS scheme.
        if ($scheme != 'https'){
            $uri->setScheme('https');
            $response=$event->getResponse();
            $response->getHeaders()->addHeaderLine('Location', $uri);
            $response->setStatusCode(301);
            $response->sendHeaders();
            return $response;
        }
    }
}
