<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class DistrictRepository extends EntityRepository
{
    public function findOneByUrl($url)
    {
        return $this->findOneBy(array('url'=>$url), array('title' => 'ASC'));
    }
}