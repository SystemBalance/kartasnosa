<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\House;

/**
 * This is the custom repository class for House entity.
 */
class HouseRepository extends EntityRepository
{

    public function findOneByIdHouse($Id)
    {
        $entityManager = $this->getEntityManager();
        return $entityManager->getRepository(House::class)->findOneBy(array('idHouse'=>$Id));
    }

    public function findOneByUrl($url)
    {
        return $this->findOneBy(array('url'=>$url), array('title' => 'ASC'));
    }

    /**
     * @param $idStreets
     * @return array
     */
    public function findByIdStreet($idStreets)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('h')
            ->from(House::class, 'h')
            ->where('h.idStreet IN ('.implode(',',$idStreets).')')
            ->orderBy('h.floors','ASC')
            ->setMaxResults(5000);
        $Houses = $queryBuilder->getQuery()->getResult();

        return $Houses;
    }

    /**
     * @param $idStreets
     * @return array
     */
    public function findByIdStreetRen($idStreets)
    {
        $entityManager = $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('h')
            ->from(House::class, 'h')
            ->where('h.idStreet IN ('.implode(',',$idStreets).')')
            ->andWhere('h.ren=1')
            ->orderBy('h.floors','ASC')
            ->setMaxResults(5000);
        $Houses = $queryBuilder->getQuery()->getResult();

        return $Houses;
    }

    /**
     * @param $idStreets
     * @return array
     */
    public function findByIdStreetNoRen($idStreets)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('h')
            ->from(House::class, 'h')
            ->where('h.idStreet IN ('.implode(',',$idStreets).')')
            ->andWhere('h.ren=0')
            ->orderBy('h.floors','ASC')
            ->setMaxResults(5000);
        $Houses = $queryBuilder->getQuery()->getResult();

        return $Houses;
    }
    /**
     * @return array
     */
    public function findAllRen()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('h')
            ->from(House::class, 'h')
            ->where('h.ren=1')
            ->orderBy('h.floors','ASC')
            ->setMaxResults(5000);
        $Houses = $queryBuilder->getQuery()->getResult();

        return $Houses;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('h')
            ->from(House::class, 'h')
            ->orderBy('h.floors','ASC')
            ->setMaxResults(5000);
        $Houses = $queryBuilder->getQuery()->getResult();

        return $Houses;
    }




    // Main
    public function getCountAll(){
        return $this->getEntityManager()->createQueryBuilder('h')->select('COUNT(h.idHouse)')->from(House::class, 'h')
            ->getQuery()->getSingleScalarResult();
    }

    public function getCountAllRen(){
        return $this->getEntityManager()->createQueryBuilder('h')->select('COUNT(h.idHouse)')->from(House::class, 'h')
            ->where('h.ren = 1')
            ->getQuery()->getSingleScalarResult();
    }
    
    // By StreetList
    protected function getCountByStreetList($streetList,$type='All'){
        $q = $this->getEntityManager()->createQueryBuilder('h')->select('COUNT(h.idHouse)')->from(House::class, 'h')
            ->where('h.idStreet IN ('.implode(',',$streetList).')')
            ->orderBy('h.floors','ASC');

        switch ($type){
            case 'Ren': $q->andWhere('h.ren = 1');break;
            case 'Ren5': $q->andWhere('h.ren = 1');$q->andWhere('h.floors = 5');break;
            case 'RenNo5': $q->andWhere('h.ren = 1');$q->andWhere('h.floors != 5');break;
        }

        return $q->getQuery()->getSingleScalarResult();
    }



    public function getCountDistrictAll($streetList){return $this->getCountByStreetList($streetList);}
    public function getCountDistrictRen($streetList){return $this->getCountByStreetList($streetList,'Ren');}
    public function getCountDistrictRen5Floor($streetList){return $this->getCountByStreetList($streetList,'Ren5');}
    public function getCountDistrictRenNo5Floor($streetList){return $this->getCountByStreetList($streetList,'RenNo5');}

    public function getCountRegionAll($streetList){return $this->getCountByStreetList($streetList);}
    public function getCountRegionRen($streetList){return $this->getCountByStreetList($streetList,'Ren');}
    public function getCountRegionRen5Floor($streetList){return $this->getCountByStreetList($streetList,'Ren5');}
    public function getCountRegionRenNo5Floor($streetList){return $this->getCountByStreetList($streetList,'RenNo5');}



    // Street
    protected function getCountStreet($idStreet,$type='All'){
        $q = $this->getEntityManager()->createQueryBuilder('h')->select('COUNT(h.idHouse)')->from(House::class, 'h')
            ->where('h.idStreet = '.$idStreet)
            ->orderBy('h.floors','ASC');

        switch ($type){
            case 'Ren': $q->andWhere('h.ren = 1');break;
            case 'Ren5': $q->andWhere('h.ren = 1');$q->andWhere('h.floors = 5');break;
            case 'RenNo5': $q->andWhere('h.ren = 1');$q->andWhere('h.floors != 5');break;
        }
        return $q->getQuery()->getSingleScalarResult();
    }

    public function getCountStreetAll($idStreet){return $this->getCountStreet($idStreet);}
    public function getCountStreetRen($idStreet){return $this->getCountStreet($idStreet,'Ren');}
    public function getCountStreetRen5Floor($idStreet){return $this->getCountStreet($idStreet,'Ren5');}
    public function getCountStreetRenNo5Floor($idStreet){return $this->getCountStreet($idStreet,'RenNo5');}

}