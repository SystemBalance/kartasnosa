<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class RegionRepository extends EntityRepository
{
    public function findOneByUrl($url)
    {
        return $this->findBy(array('url'=>$url), array('title' => 'ASC'));
    }

//    public function getStreets(){
//        return $this->findBy(array('url'=>$url), array('title' => 'ASC'));
//    }

//    public function findByRegions($regions)
//    {
//        $entityManager = $this->getEntityManager();
//
//        $queryBuilder = $entityManager->createQueryBuilder();
//
//        $queryBuilder->select('s')
//            ->from(Street::class, 's')
//            ->where('s.idRegion IN (?1)')
//            ->orderBy('s.title','ASC')
//            ->setParameter('1', implode(',',$regions));
//
//        $Streets = $queryBuilder->getQuery()->getResult();
//
//        return $Streets;
//    }
}