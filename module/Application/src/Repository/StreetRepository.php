<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\Street;
use Zend\Hydrator\HydrationInterface;

class StreetRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), ['title' => 'ASC']);
    }

    public function findByRegions($regions)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('s')
            ->from(Street::class, 's')
            ->where('s.idRegion IN ('.implode(',',$regions).')')
            ->orderBy('s.title','ASC');
//            ->setParameter('1', );

        $Streets = $queryBuilder->getQuery()->getResult();

        return $Streets;
    }

    public function findOneByUrl($url)
    {
        return $this->findOneBy(array('url'=>$url), array('title' => 'ASC'));
    }

    public function getJsonList()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('s')
            ->from(Street::class, 's')
            ->orderBy('s.title','ASC');
        $Streets = $queryBuilder->getQuery()->getResult();

        return $Streets;
    }

    public function getIdListByRegionsId($regions)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();
        $queryBuilder->select(['s.idStreet'])
            ->from(Street::class, 's')
            ->where('s.idRegion IN ('.implode(',',$regions).')')
            ->orderBy('s.title','ASC');
        $Streets = $queryBuilder->getQuery()->getResult();
        $streetList = [];
        foreach ($Streets as $s){
            $streetList[]= $s['idStreet'];
        }
        return $streetList;
    }
//
//    public function getHousesRen(){
//        $em = $this->getEntityManager();
//        $qb = $em->getRepository('House')->
//    }
    
}