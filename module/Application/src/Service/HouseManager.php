<?php
namespace Application\Service;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Application\Entity\House;
use Application\Entity\Comment;
use Application\Entity\Tag;
use Zend\Filter\StaticFilter;

/**
 * The HouseManager service is responsible for adding new House, updating existing
 * House, adding tags to House, etc.
 */
class HouseManager
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager;
     */
    private $entityManager;
    
    /**
     * Constructor.
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * This method adds a new House.
     */
    public function addNewHouse($data) 
    {
        // Create new House entity.
        $House = new House();
        $House->setTitle($data['title']);
        $House->setContent($data['content']);
        $House->setStatus($data['status']);
        $currentDate = date('Y-m-d H:i:s');
        $House->setDateCreated($currentDate);        
        
        // Add the entity to entity manager.
        $this->entityManager->persist($House);
        
        // Add tags to House
        $this->addTagsToHouse($data['tags'], $House);
        
        // Apply changes to database.
        $this->entityManager->flush();
    }
    
    /**
     * This method allows to update data of a single House.
     */
    public function updateHouse($House, $data) 
    {
        $House->setTitle($data['title']);
        $House->setContent($data['content']);
        $House->setStatus($data['status']);
        
        // Add tags to House
        $this->addTagsToHouse($data['tags'], $House);
        
        // Apply changes to database.
        $this->entityManager->flush();
    }

    /**
     * Adds/updates tags in the given House.
     */
    private function addTagsToHouse($tagsStr, $House) 
    {
        // Remove tag associations (if any)
//        $tags = $House->getTags();
//        foreach ($tags as $tag) {
//            $House->removeTagAssociation($tag);
//        }
        
        // Add tags to House
        $tags = explode(',', $tagsStr);
        foreach ($tags as $tagName) {
            
            $tagName = StaticFilter::execute($tagName, 'StringTrim');
            if (empty($tagName)) {
                continue; 
            }
            
            $tag = $this->entityManager->getRepository(Tag::class)
                    ->findOneByName($tagName);
            if ($tag == null)
                $tag = new Tag();
            
            $tag->setName($tagName);
            $tag->addHouse($House);
            
            $this->entityManager->persist($tag);
            
            $House->addTag($tag);
        }
    }    
    
    /**
     * Returns status as a string.
     */
    public function getHouseStatusAsString($House) 
    {
        switch ($House->getStatus()) {
            case House::STATUS_DRAFT: return 'Draft';
            case House::STATUS_PUBLISHED: return 'Published';
        }
        
        return 'Unknown';
    }
    
    /**
     * Converts tags of the given House to comma separated list (string).
     */
    public function convertTagsToString($House) 
    {
        $tags = [];//$House->getTags();
        $tagCount = count($tags);
        $tagsStr = '';
        $i = 0;
        foreach ($tags as $tag) {
            $i ++;
            $tagsStr .= $tag->getName();
            if ($i < $tagCount) 
                $tagsStr .= ', ';
        }
        
        return $tagsStr;
    }    

    /**
     * Returns count of comments for given House as properly formatted string.
     */
    public function getCommentCountStr($House)
    {
        $commentCount = count($House->getComments());
        if ($commentCount == 0)
            return 'No comments';
        else if ($commentCount == 1) 
            return '1 comment';
        else
            return $commentCount . ' comments';
    }


    /**
     * This method adds a new comment to House.
     */
    public function addCommentToHouse($House, $data) 
    {
        // Create new Comment entity.
        $comment = new Comment();
        $comment->setHouse($House);
        $comment->setAuthor($data['author']);
        $comment->setContent($data['comment']);        
        $currentDate = date('Y-m-d H:i:s');
        $comment->setDateCreated($currentDate);

        // Add the entity to entity manager.
        $this->entityManager->persist($comment);

        // Apply changes.
        $this->entityManager->flush();
    }
    
    /**
     * Removes House and all associated comments.
     */
    public function removeHouse($House) 
    {
        // Remove associated comments
        $comments = $House->getComments();
        foreach ($comments as $comment) {
            $this->entityManager->remove($comment);
        }
        
        // Remove tag associations (if any)
        $tags = $House->getTags();
        foreach ($tags as $tag) {
            
            $House->removeTagAssociation($tag);
        }
        
        $this->entityManager->remove($House);
        
        $this->entityManager->flush();
    }
    
    /**
     * Calculates frequencies of tag usage.
     */
    public function getTagCloud()
    {
        $tagCloud = [];
                
        $House = $this->entityManager->getRepository(House::class)
                    ->findHouseHavingAnyTag();
        $totalHouseCount = count($House);
        
        $tags = $this->entityManager->getRepository(Tag::class)
                ->findAll();
        foreach ($tags as $tag) {
            
            $HouseByTag = $this->entityManager->getRepository(House::class)
                    ->findHouseByTag($tag->getName())->getResult();
            
            $HouseCount = count($HouseByTag);
            if ($HouseCount > 0) {
                $tagCloud[$tag->getName()] = $HouseCount;
            }
        }
        
        $normalizedTagCloud = [];
        
        // Normalize
        foreach ($tagCloud as $name=>$HouseCount) {
            $normalizedTagCloud[$name] =  $HouseCount/$totalHouseCount;
        }
        
        return $normalizedTagCloud;
    }
}



