<?php
/**
 * Created by PhpStorm.
 * User: Smuzi
 * Date: 30.05.2017
 * Time: 8:14
 */




$geocode = file_get_contents('http://geocode-maps.yandex.ru/1.x/?geocode='. urlencode('Москва') .'');
$xml = new SimpleXMLElement($geocode);

$xml->registerXPathNamespace('ymaps', 'http://maps.yandex.ru/ymaps/1.x');
$xml->registerXPathNamespace('gml', 'http://www.opengis.net/gml');

$result = $xml->xpath('/ymaps:ymaps/ymaps:GeoObjectCollection/gml:featureMember/ymaps:GeoObject/gml:Point/gml:pos');

echo($result[0]);


?>
<sctipt>
    var t = {"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"Тверская
    6","found":"100","results":"10"}},"featureMember":[{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия,
    Москва, Тверская улица, 6с1","precision":"exact","Address":{"country_code":"RU","formatted":"Москва, Тверская улица,
    6с1","Components":[{"kind":"country","name":"Россия"},{"kind":"province","name":"Центральный федеральный
    округ"},{"kind":"province","name":"Москва"},{"kind":"locality","name":"Москва"},{"kind":"street","name":"Тверская
    улица"},{"kind":"house","name":"6с1"}]},"AddressDetails":{"Country":{"AddressLine":"Москва, Тверская улица,
    6с1","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Москва","Locality":{"LocalityName":"Москва","Thoroughfare":{"ThoroughfareName":"Тверская
    улица","Premise":{"PremiseNumber":"6с1"}}}}}}}},"description":"Москва, Россия","name":"Тверская улица,
    6с1","boundedBy":{"Envelope":{"lowerCorner":"37.60724173 55.75792615","upperCorner":"37.61545234
    55.76255576"}},"Point":{"pos":"37.611347
    55.760241"}}},{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Санкт-Петербург,
    Тверская улица, 6","precision":"exact","Address":{"country_code":"RU","formatted":"Санкт-Петербург, Тверская улица,
    6","Components":[{"kind":"country","name":"Россия"},{"kind":"province","name":"Северо-Западный федеральный
    округ"},{"kind":"province","name":"Санкт-Петербург"},{"kind":"locality","name":"Санкт-Петербург"},{"kind":"street","name":"Тверская
    улица"},{"kind":"house","name":"6"}]},"AddressDetails":{"Country":{"AddressLine":"Санкт-Петербург, Тверская улица,
    6","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Санкт-Петербург","Locality":{"LocalityName":"Санкт-Петербург","Thoroughfare":{"ThoroughfareName":"Тверская
    улица","Premise":{"PremiseNumber":"6"}}}}}}}},"description":"Санкт-Петербург, Россия","name":"Тверская улица,
    6","boundedBy":{"Envelope":{"lowerCorner":"30.37572285 59.94512535","upperCorner":"30.38393345
    59.94924416"}},"Point":{"pos":"30.379828
    59.947185"}}},{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Украина, Киев, Тверская
    улица, 6","precision":"exact","Address":{"country_code":"UA","formatted":"Киев, Тверская улица,
    6","Components":[{"kind":"country","name":"Украина"},{"kind":"province","name":"Киев"},{"kind":"locality","name":"Киев"},{"kind":"street","name":"Тверская
    улица"},{"kind":"house","name":"6"}]},"AddressDetails":{"Country":{"AddressLine":"Киев, Тверская улица,
    6","CountryNameCode":"UA","CountryName":"Украина","AdministrativeArea":{"AdministrativeAreaName":"Киев","Locality":{"LocalityName":"Киев","Thoroughfare":{"ThoroughfareName":"Тверская
    улица","Premise":{"PremiseNumber":"6"}}}}}}}},"description":"Киев, Украина","name":"Тверская улица,
    6","boundedBy":{"Envelope":{"lowerCorner":"30.51904007 50.41708244","upperCorner":"30.52725067
    50.42232821"}},"Point":{"pos":"30.523145
    50.419705"}}},{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Санкт-Петербург,
    Колпино, Тверская улица, 6","precision":"exact","Address":{"country_code":"RU","formatted":"Санкт-Петербург,
    Колпино, Тверская улица,
    6","Components":[{"kind":"country","name":"Россия"},{"kind":"province","name":"Северо-Западный федеральный
    округ"},{"kind":"province","name":"Санкт-Петербург"},{"kind":"area","name":"Колпинский
    район"},{"kind":"locality","name":"Колпино"},{"kind":"street","name":"Тверская
    улица"},{"kind":"house","name":"6"}]},"AddressDetails":{"Country":{"AddressLine":"Санкт-Петербург, Колпино, Тверская
    улица,
    6","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Санкт-Петербург","SubAdministrativeArea":{"SubAdministrativeAreaName":"Колпинский
    район","Locality":{"LocalityName":"Колпино","Thoroughfare":{"ThoroughfareName":"Тверская
    улица","Premise":{"PremiseNumber":"6"}}}}}}}}},"description":"Колпино, Санкт-Петербург, Россия","name":"Тверская
    улица, 6","boundedBy":{"Envelope":{"lowerCorner":"30.60666174 59.74387563","upperCorner":"30.61487234
    59.7480195"}},"Point":{"pos":"30.610767
    59.745948"}}},{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Москва, 1-я
    Тверская-Ямская улица, 6","precision":"exact","Address":{"country_code":"RU","formatted":"Москва, 1-я
    Тверская-Ямская улица, 6","Components":[{"kind":"country","name":"Россия"},{"kind":"province","name":"Центральный
    федеральный
    округ"},{"kind":"province","name":"Москва"},{"kind":"locality","name":"Москва"},{"kind":"street","name":"1-я
    Тверская-Ямская улица"},{"kind":"house","name":"6"}]},"AddressDetails":{"Country":{"AddressLine":"Москва, 1-я
    Тверская-Ямская улица,
    6","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Москва","Locality":{"LocalityName":"Москва","Thoroughfare":{"ThoroughfareName":"1-я
    Тверская-Ямская улица","Premise":{"PremiseNumber":"6"}}}}}}}},"description":"Москва, Россия","name":"1-я
    Тверская-Ямская улица, 6","boundedBy":{"Envelope":{"lowerCorner":"37.58985933
    55.76926113","upperCorner":"37.59806994 55.77388939"}},"Point":{"pos":"37.593965
    55.771575"}}},{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Украина, Днепр, Тверская
    улица, 6","precision":"exact","Address":{"country_code":"UA","formatted":"Днепр, Тверская улица,
    6","Components":[{"kind":"country","name":"Украина"},{"kind":"province","name":"Днепропетровская
    область"},{"kind":"area","name":"Днепровский городской
    совет"},{"kind":"locality","name":"Днепр"},{"kind":"street","name":"Тверская
    улица"},{"kind":"house","name":"6"}]},"AddressDetails":{"Country":{"AddressLine":"Днепр, Тверская улица,
    6","CountryNameCode":"UA","CountryName":"Украина","AdministrativeArea":{"AdministrativeAreaName":"Днепропетровская
    область","SubAdministrativeArea":{"SubAdministrativeAreaName":"Днепровский городской
    совет","Locality":{"LocalityName":"Днепр","Thoroughfare":{"ThoroughfareName":"Тверская
    улица","Premise":{"PremiseNumber":"6"}}}}}}}}},"description":"Днепр, Украина","name":"Тверская улица,
    6","boundedBy":{"Envelope":{"lowerCorner":"35.07617554 48.50496667","upperCorner":"35.08438614
    48.51042245"}},"Point":{"pos":"35.080281
    48.507695"}}},{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Московская
    область, Дубна, Тверская улица, 6","precision":"exact","Address":{"country_code":"RU","formatted":"Московская
    область, Дубна, Тверская улица,
    6","Components":[{"kind":"country","name":"Россия"},{"kind":"province","name":"Центральный федеральный
    округ"},{"kind":"province","name":"Московская область"},{"kind":"area","name":"городской округ
    Дубна"},{"kind":"locality","name":"Дубна"},{"kind":"street","name":"Тверская
    улица"},{"kind":"house","name":"6"}]},"AddressDetails":{"Country":{"AddressLine":"Московская область, Дубна,
    Тверская улица,
    6","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Московская
    область","SubAdministrativeArea":{"SubAdministrativeAreaName":"городской округ
    Дубна","Locality":{"LocalityName":"Дубна","Thoroughfare":{"ThoroughfareName":"Тверская
    улица","Premise":{"PremiseNumber":"6"}}}}}}}}},"description":"Дубна, Московская область, Россия","name":"Тверская
    улица, 6","boundedBy":{"Envelope":{"lowerCorner":"37.13577892 56.76047031","upperCorner":"37.14398953
    56.76497972"}},"Point":{"pos":"37.139884
    56.762725"}}},{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Тверь, Тверская
    площадь, 6","precision":"exact","Address":{"country_code":"RU","formatted":"Тверь, Тверская площадь,
    6","Components":[{"kind":"country","name":"Россия"},{"kind":"province","name":"Центральный федеральный
    округ"},{"kind":"province","name":"Тверская область"},{"kind":"area","name":"городской округ
    Тверь"},{"kind":"locality","name":"Тверь"},{"kind":"street","name":"Тверская
    площадь"},{"kind":"house","name":"6"}]},"AddressDetails":{"Country":{"AddressLine":"Тверь, Тверская площадь,
    6","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Тверская
    область","SubAdministrativeArea":{"SubAdministrativeAreaName":"городской округ
    Тверь","Locality":{"LocalityName":"Тверь","Thoroughfare":{"ThoroughfareName":"Тверская
    площадь","Premise":{"PremiseNumber":"6"}}}}}}}}},"description":"Тверь, Россия","name":"Тверская площадь,
    6","boundedBy":{"Envelope":{"lowerCorner":"35.89883471 56.85507334","upperCorner":"35.90704531
    56.85957133"}},"Point":{"pos":"35.90294
    56.857322"}}},{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Москва, 2-я
    Тверская-Ямская улица, 6","precision":"exact","Address":{"country_code":"RU","formatted":"Москва, 2-я
    Тверская-Ямская улица, 6","Components":[{"kind":"country","name":"Россия"},{"kind":"province","name":"Центральный
    федеральный
    округ"},{"kind":"province","name":"Москва"},{"kind":"locality","name":"Москва"},{"kind":"street","name":"2-я
    Тверская-Ямская улица"},{"kind":"house","name":"6"}]},"AddressDetails":{"Country":{"AddressLine":"Москва, 2-я
    Тверская-Ямская улица,
    6","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Москва","Locality":{"LocalityName":"Москва","Thoroughfare":{"ThoroughfareName":"2-я
    Тверская-Ямская улица","Premise":{"PremiseNumber":"6"}}}}}}}},"description":"Москва, Россия","name":"2-я
    Тверская-Ямская улица, 6","boundedBy":{"Envelope":{"lowerCorner":"37.5922758 55.76874966","upperCorner":"37.6004864
    55.77337798"}},"Point":{"pos":"37.596381
    55.771064"}}},{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Московская
    область, поселок городского типа Лотошино, Тверская улица,
    6","precision":"exact","Address":{"country_code":"RU","formatted":"Московская область, поселок городского типа
    Лотошино, Тверская улица, 6","Components":[{"kind":"country","name":"Россия"},{"kind":"province","name":"Центральный
    федеральный округ"},{"kind":"province","name":"Московская область"},{"kind":"area","name":"Лотошинский
    район"},{"kind":"locality","name":"поселок городского типа Лотошино"},{"kind":"street","name":"Тверская
    улица"},{"kind":"house","name":"6"}]},"AddressDetails":{"Country":{"AddressLine":"Московская область, поселок
    городского типа Лотошино, Тверская улица,
    6","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Московская
    область","SubAdministrativeArea":{"SubAdministrativeAreaName":"Лотошинский
    район","Locality":{"LocalityName":"поселок городского типа Лотошино","Thoroughfare":{"ThoroughfareName":"Тверская
    улица","Premise":{"PremiseNumber":"6"}}}}}}}}},"description":"поселок городского типа Лотошино, Московская область,
    Россия","name":"Тверская улица, 6","boundedBy":{"Envelope":{"lowerCorner":"35.62674399
    56.23102753","upperCorner":"35.6349546 56.23560058"}},"Point":{"pos":"35.630849 56.233314"}}}]}}}
</sctipt>
